using Apex;

class Module : Apex.Application {

    bool cancel;

    public Module () {
        GLib.Object (application_id: "org.apex.TestModule",
                     flags: ApplicationFlags.FLAGS_NONE);

        Unix.signal_add (Posix.Signal.INT, () => {
            cancel = true;
            return true;
        });
    }

    public override Apex.ConfigurationResponse get_configuration () {
        var response = new Apex.ConfigurationResponse ();
        var configuration = new Apex.Configuration ();

        response.set_configuration (configuration);
        message ("%s", response.serialize ());

        return response;
    }

    public override Apex.JobResponse submit_job (string job_name) {
        Apex.JobResponse response = new Apex.JobResponse ();

        message ("do thing");

        return response;
    }

    public static int main (string[] args) {
        var app = new Module ();
        app.set_endpoint ("tcp://localhost:7201");
        app.set_service ("acquire-genicam");
        app.set_inactivity_timeout (10000);

        return app.run (args);
    }
}
